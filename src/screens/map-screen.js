import * as React from 'react'
import { View, StyleSheet, Dimensions } from 'react-native'
import MapView, { Marker } from 'react-native-maps'

const oxfordPoint = { latitude: 42.82473, longitude: -83.26499 }
export default function MapScreen() {
  return (
    <View style={styles.container}>
      <MapView
        mapType={Platform.OS == 'android' ? 'none' : 'standard'}
        camera={{
          center: oxfordPoint,
          altitude: 200 * 10000,
          pitch: 0,
          heading: 0,
          zoom: 200,
        }}
        style={styles.map}
        showsUserLocation={true}
      >
        <Marker
          key={1}
          coordinate={oxfordPoint}
          title='Oxford'
          description='Where I live'
        />
      </MapView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
  },
})
