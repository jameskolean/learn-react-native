import React, { useContext } from 'react'
import { Button, Text, View, StyleSheet } from 'react-native'
import { userProfileContext } from '../context/user-profile-context'

export default function ProfileScreen({ navigation }) {
  const { state: userProfile, dispatch } = useContext(userProfileContext)
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Profile Screen</Text>
      {!userProfile.authenticated && (
        <Button
          title='Login'
          onPress={() =>
            dispatch({
              type: 'login',
              payload: { username: 'James' },
            })
          }
        />
      )}
      {userProfile.authenticated && (
        <>
          <Text style={styles.title}>Hello {userProfile.username}</Text>
          <Button title='Logout' onPress={() => dispatch({ type: 'logout' })} />
        </>
      )}
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
})
