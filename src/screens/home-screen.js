import * as React from 'react'
import { Button, Text, View, StyleSheet } from 'react-native'

export default function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Home Screen</Text>
      <Button title='Go to Map' onPress={() => navigation.navigate('Map')} />
      <Button
        title='Go to Details'
        onPress={() =>
          navigation.navigate('Detail', {
            itemId: 86,
          })
        }
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
})
