import * as React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import usePlan from '../hooks/use-plan'
import { Picker } from '@react-native-picker/picker'

export default function SettingsScreen({ navigation }) {
  const [plan, planError, planIsLoading, setPlan] = usePlan()
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Settings Screen</Text>
      <Text>Your plan is {planIsLoading ? 'loading' : plan}</Text>
      <Text style={styles.title}>Choose a plan</Text>
      <Picker
        selectedValue={plan}
        style={{ height: 100, width: 200 }}
        onValueChange={(itemValue, itemIndex) => setPlan(itemValue)}
      >
        <Picker.Item label='Community' value='community' />
        <Picker.Item label='Developer' value='developer' />
        <Picker.Item label='Pro' value='pro' />
      </Picker>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
})
