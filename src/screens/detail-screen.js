import * as React from 'react'
import { Button, Text, View, StyleSheet } from 'react-native'

export default function DetailScreen({ navigation, route }) {
  const { itemId } = route.params
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Detail</Text>
      <Text style={styles.title}>for item: {itemId}</Text>
      <Button
        title='Go to Details... again'
        onPress={() =>
          navigation.push('Detail', {
            itemId: Math.floor(Math.random() * 100),
          })
        }
      />
      <Button title='Go back' onPress={() => navigation.goBack()} />
      <Button title='Go to Home' onPress={() => navigation.navigate('Home')} />
      <Button
        title='Go back to first screen in stack'
        onPress={() => navigation.popToTop()}
      />
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
})
