//@ts-check
import * as React from 'react'

import { Text, TextProps } from 'react-native'

/**
 *
 * @param {TextProps} props
 */
export function MonoText(props) {
  return <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />
}
