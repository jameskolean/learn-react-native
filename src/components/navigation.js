//@ts-check
import React from 'react'
import { Button } from 'react-native'

import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'

import HomeScreen from '../screens/home-screen'
import DetailScreen from '../screens/detail-screen'
import ProfileScreen from '../screens/profile-screen'
import SettingsScreen from '../screens/settings-screen'
import MapScreen from '../screens/map-screen'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()
const Drawer = createDrawerNavigator()

export const MainDrawerNavigator = () => (
  <Drawer.Navigator initialRouteName='Home'>
    <Drawer.Screen name='Home' component={BottomTabNavigator} />
    <Drawer.Screen name='Profile' component={ProfileStackNavigator} />
  </Drawer.Navigator>
)
export const BottomTabNavigator = () => (
  <Tab.Navigator>
    <Tab.Screen name='Home' component={HomeStackNavigator} />
    <Tab.Screen name='Settings' component={SettingsStackNavigator} />
  </Tab.Navigator>
)
export const ProfileStackNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name='Profile' component={ProfileScreen} />
  </Stack.Navigator>
)
export const SettingsStackNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name='Setttings' component={SettingsScreen} />
  </Stack.Navigator>
)

export const HomeStackNavigator = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#f4511e',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
        headerRight: () => (
          <Button
            onPress={() => alert('This is a button!')}
            title='Info'
            color='#fff'
          />
        ),
      }}
    >
      <Stack.Screen name='Home' component={HomeScreen} />
      <Stack.Screen name='Detail' component={DetailScreen} />
      <Stack.Screen name='Map' component={MapScreen} />
    </Stack.Navigator>
  )
}
