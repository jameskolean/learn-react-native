import React, { createContext, useReducer } from 'react'

const initialState = {
  username: 'unknown',
  email: '',
  authenticated: false,
  photoUrl: '/anonymous.jpg',
}
const userProfileContext = createContext(initialState)

const UserProfileProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case 'login': {
        const newState = {
          ...state,
          username: action.payload.username,
          authenticated: true,
        }
        return newState
      }
      case 'logout':
        return { ...initialState }
      default:
        throw new Error()
    }
  }, initialState)

  return (
    <userProfileContext.Provider value={{ state, dispatch }}>
      {children}
    </userProfileContext.Provider>
  )
}

export { userProfileContext, UserProfileProvider }
