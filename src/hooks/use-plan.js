import { useState, useEffect } from 'react'
import AsyncStorage from '@react-native-async-storage/async-storage'

export default function usePlan() {
  const [value, setValue] = useState('')
  const [error, setError] = useState(null)
  const [isLoading, setIsLoading] = useState(true)
  async function getPlan() {
    try {
      setIsLoading(true)
      const value = await AsyncStorage.getItem('@plan')
      setValue(value == null ? 'community' : value)
    } catch (e) {
      setError(e)
    } finally {
      setIsLoading(false)
    }
  }
  async function setPlan(value) {
    try {
      const theme = await AsyncStorage.setItem('@plan', value)
      setValue(value)
    } catch (e) {
      setError(e)
    }
  }

  useEffect(() => {
    getPlan()
  }, [])

  return [value, error, isLoading, setPlan]
}
