//@ts-check
import React from 'react'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { StyleSheet } from 'react-native'

import useCachedResources from './src/hooks/useCachedResources'
import { NavigationContainer } from '@react-navigation/native'

import { MainDrawerNavigator } from './src/components/navigation'

import { UserProfileProvider } from './src/context/user-profile-context'

export default function App() {
  const isLoadingComplete = useCachedResources()
  if (!isLoadingComplete) {
    return null
  } else {
    return (
      <UserProfileProvider>
        <NavigationContainer>
          <MainDrawerNavigator />
        </NavigationContainer>
      </UserProfileProvider>
    )
  }
}

const styles = StyleSheet.create({})
